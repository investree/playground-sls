/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.config;

import org.springframework.beans.factory.annotation.Value;

/**
 * AliyunSLSConfiguration
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:42:44 PM
 */
public class AliyunSLSConfiguration extends AliyunConfiguration {

    @Value("${aliyun.sls.project}")
    protected String project;
    
    @Value("${aliyun.sls.logStore}")
    protected String logStore;

    @Override
    public String toString() {
        return super.toString() + "\nProject : " + project + "\nLogStore : " + logStore;
    }
}