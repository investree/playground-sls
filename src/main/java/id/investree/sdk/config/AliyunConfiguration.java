/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.config;

import com.aliyun.openservices.log.Client;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;

/**
 * AliyunConfiguration
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:40:23 PM
 */
public class AliyunConfiguration {

    @Value("${aliyun.accessId}")
    protected String accessId;

    @Value("${aliyun.accessKey}")
    protected String accessKey;

    @Value("${aliyun.endpoint}")
    protected String endpoint;

    protected Client client;

    @PostConstruct
    private void postConstruct() {
        client = new Client(endpoint, accessId, accessKey);
    }

    @Override
    public String toString() {
        return "Access Id : " + accessId + "\nAccess Key : " + accessKey + "\nEndpoint : " + endpoint;
    }
}
