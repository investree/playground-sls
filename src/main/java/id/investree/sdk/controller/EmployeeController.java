/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.controller;

import id.investree.sdk.entity.Employee;
import id.investree.sdk.logger.AliyunSLS;
import id.investree.sdk.logger.AliyunSLSFactory;
import id.investree.sdk.rest.ResponseWrapper;
import id.investree.sdk.services.EmployeeService;
import java.io.Serializable;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * EmployeeController
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:07:47 AM
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController implements Serializable {

    private static final long serialVersionUID = -6221540419448363740L;
    
    private static final AliyunSLS logger = AliyunSLSFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService service;

    @GetMapping("/list")
    public ResponseWrapper<List<Employee>> list(HttpServletResponse response) {
        response.addCookie(new Cookie("TestCookieKey1", "TestCookieValue1"));
        response.addCookie(new Cookie("TestCookieKey2", "TestCookieValue2"));
        
        response.setHeader("TestHeaderKey1", "TestHeaderValue1");
        response.setHeader("TestHeaderKey2", "TestHeaderValue2");
        
        logger.info("EmployeeTopic", "Starting get list employee..");
        logger.info("EmployeeTopic", "Query to database...");
        List<Employee> list = service.findAll();
        logger.info("EmployeeTopic", "Finish..");
        return new ResponseWrapper<List<Employee>>(1, "success", null, list);
    }

    @GetMapping("/detail/{id}")
    public ResponseWrapper<Employee> detail(@PathVariable("id") int id) {
        logger.info("EmployeeTopic", "Starting get detail employee..");
        logger.info("EmployeeTopic", "Query to database...");
        Employee emp = service.findById(id);
        logger.info("EmployeeTopic", "Finish..");
        return new ResponseWrapper<Employee>(1, "success", null, emp);
    }

    @PostMapping("/save")
    public ResponseWrapper<Employee> save(@RequestBody Employee emp) {
        logger.info("EmployeeTopic", "Starting save data employee..");
        logger.info("EmployeeTopic", "Save to database...");
        service.save(emp);
        logger.info("EmployeeTopic", "Finish..");
        
        return new ResponseWrapper<Employee>(1, "success", "Save sucessfully", emp);
    }
}
