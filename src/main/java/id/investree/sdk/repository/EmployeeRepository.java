/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.repository;

import id.investree.sdk.entity.Employee;
import org.springframework.stereotype.Repository;

/**
 * EmployeeRepository
 *
 * @author Fariz Fadian
 * @since May 27, 2020 - 12:13:15 AM
 */
@Repository
public class EmployeeRepository extends BaseRepository<Employee> {
    
}