/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.repository;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceContext;
import org.hibernate.jpa.QueryHints;

/**
 * BaseRepository
 *
 * @author Fariz Fadian
 * @since May 27, 2020 - 12:07:07 AM
 */
@MappedSuperclass
public class BaseRepository<E> {

    @PersistenceContext
    private EntityManager em;

    private String entityClassName;

    @PostConstruct
    private void init() {
        entityClassName = ((Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public final void save(E entity) {
        getEntityManager().persist(entity);
        getEntityManager().flush();
    }

    public final E findById(int id) {
        return (E) getEntityManager()
                .createQuery("SELECT t FROM " + getEntityClassName() + " t WHERE t.id = :id")
                .setParameter("id", id)
                .setHint(QueryHints.HINT_READONLY, true)
                .setMaxResults(1)
                .getSingleResult();
    }
    
    public final List<E> findAll() {
        return getEntityManager().createQuery("SELECT t FROM " + getEntityClassName() + " t").getResultList();
    }

}
