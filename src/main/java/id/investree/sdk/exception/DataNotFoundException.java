/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.exception;

/**
 * DataNotFoundException
 *
 * @author Fariz Fadian
 * @since May 29, 2020 - 3:51:53 PM
 */
public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(int id) {
        super("Data Not Found : " + id);
    }

    public DataNotFoundException() {
    }
}