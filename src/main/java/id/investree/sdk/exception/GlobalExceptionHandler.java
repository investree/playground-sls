/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.exception;

import id.investree.sdk.rest.ResponseWrapper;
import javax.persistence.NoResultException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * GlobalExceptionHandler
 *
 * @author Fariz Fadian
 * @since May 29, 2020 - 3:54:27 PM
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
        DataNotFoundException.class,
        NoResultException.class,
        EmptyResultDataAccessException.class
    })
    public ResponseEntity<ResponseWrapper> customHandleNotFound(Exception ex, WebRequest request) {

        ResponseWrapper response = new ResponseWrapper();
        response.setCode(404);
        response.setStatus("error");
        response.setMessage(ex.getMessage());

        return new ResponseEntity<ResponseWrapper>(response, HttpStatus.NOT_FOUND);
    }
}
