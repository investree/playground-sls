/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.servlet.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.input.TeeInputStream;

/**
 * RequestWrapper
 * 
 * @author Fariz Fadian
 * @since May 29, 2020 - 11:35:03 AM
 */
public class RequestWrapper extends HttpServletRequestWrapper {

    private ByteArrayOutputStream baos;

    public RequestWrapper(HttpServletRequest request) {
        super(request);
        this.baos = new ByteArrayOutputStream();
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new ServletInputStream() {

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {
            }

            private TeeInputStream tee = new TeeInputStream(RequestWrapper.super.getInputStream(), baos);

            @Override
            public int read() throws IOException {
                return tee.read();
            }
        };
    }

    public byte[] toByteArray() {
        return baos.toByteArray();
    }

}
