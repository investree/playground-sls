/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.servlet;

import id.investree.sdk.servlet.filter.ResponseWrapper;
import id.investree.sdk.servlet.filter.RequestWrapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;

/**
 * ServletUtils
 *
 * @author Fariz Fadian
 * @since May 27, 2020 - 1:59:09 AM
 */
public final class ServletUtils {

    public static final String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if(queryString == null)
            return requestURL.toString();
        else
            return requestURL.append('?').append(queryString).toString();
    }

    public static final String getRequestBody(HttpServletRequest request) throws IOException {
        if("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }
    
    public static final String getRequestBody(RequestWrapper requestWrapper) throws UnsupportedEncodingException {
        String characterEncoding = requestWrapper.getCharacterEncoding() != null ? requestWrapper.getCharacterEncoding() : "UTF-8";
        return new String(requestWrapper.toByteArray(), characterEncoding);
    }
    
    public static final String getResponseBody(ResponseWrapper responseWrapper) throws UnsupportedEncodingException {
        return new String(responseWrapper.toByteArray(), responseWrapper.getCharacterEncoding());
    }

    public static final String getClientIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if(ipAddress == null)
            ipAddress = request.getRemoteAddr();
        if(ipAddress.startsWith("0:0:0:0"))
            ipAddress = "localhost";
        return ipAddress;
    }
}
