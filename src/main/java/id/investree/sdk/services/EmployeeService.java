/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.services;

import id.investree.sdk.entity.Employee;
import id.investree.sdk.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * EmployeeService
 *
 * @author Fariz Fadian
 * @since May 27, 2020 - 12:16:29 AM
 */
@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Transactional(readOnly = true)
    public List<Employee> findAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Employee findById(int id) {
        return repository.findById(id);
    }

    public void save(Employee employee) {
        repository.save(employee);
    }
}
