/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.logger;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * AsyncAliyunSLS
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:55:27 PM
 */
@Component
public class AsyncAliyunSLS extends BaseAliyunSLS {
    
    private AliyunSLS aliyunSLS;

    protected void setAliyunSLS(AliyunSLS aliyunSLS) {
        this.aliyunSLS = aliyunSLS;
    }

    @Override
    protected void setClazz(Class<?> clazz) {
        super.setClazz(clazz);
        aliyunSLS = AliyunSLSFactory.getLogger(clazz);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void debug(String topic, String message) {
        aliyunSLS.debug(topic, message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void debug(String message) {
        aliyunSLS.debug("", message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void info(String topic, String message) {
        aliyunSLS.info(topic, message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void info(String message) {
        aliyunSLS.info(message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void warn(String topic, String message) {
        aliyunSLS.warn(topic, message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void warn(String message) {
        aliyunSLS.warn("", message);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void error(String topic, String message, Throwable t) {
        aliyunSLS.error(topic, message, t);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void error(String message, Throwable t) {
        aliyunSLS.error("", message, t);
    }

    @Async("aliyunSLSTaskExecutor")
    @Override
    public void error(Throwable t) {
        aliyunSLS.error("", t.getMessage(), t);
    }

}
