/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.logger;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import id.investree.sdk.servlet.ServletUtils;
import id.investree.sdk.servlet.filter.RequestWrapper;
import id.investree.sdk.servlet.filter.ResponseWrapper;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

/**
 * HttpAuditLog
 *
 * @author Fariz Fadian
 * @since May 29, 2020 - 11:33:44 AM
 */
@WebFilter(urlPatterns = {"/*"})
@Component
public class HttpAuditLog implements Filter {

    private static final AliyunSLS logger = AliyunSLSFactory.getLogger(HttpAuditLog.class);

    private static final void logRequest(final RequestWrapper request) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n---------------------------------");
        sb.append("\nHTTP REQUEST");
        sb.append("\n---------------------------------");
        sb.append("\n--> URL                : " + ServletUtils.getFullURL(request));
        sb.append("\n--> Method             : " + request.getMethod());

        try {
            String requestBody = ServletUtils.getRequestBody(request);
            if(requestBody != null && !requestBody.isEmpty())
                sb.append("\n--> Request Body       : " + requestBody);
        }
        catch(Exception e) {
        }

        // Print Request Parameter
        Map<String, String[]> params = request.getParameterMap();
        if(params != null && params.size() > 0) {
            sb.append("\n--> Request Parameters :");
            for(Map.Entry<String, String[]> param : params.entrySet()) {
                sb.append("\n    - " + param.getKey() + " : ");

                StringBuilder sb2 = new StringBuilder();
                for(String val : param.getValue())
                    sb2.append(val + ", ");

                if(sb2.length() > 2)
                    sb.append(sb2.substring(0, sb2.length() - 2));
            }
        }

        Enumeration<String> headerNames = request.getHeaderNames();
        if(headerNames != null) {
            sb.append("\n--> Headers :");
            String key = null;
            while(headerNames.hasMoreElements()) {
                key = headerNames.nextElement();
                sb.append("\n    - " + padRight(key, 30) + " : " + request.getHeader(key));
            }
        }

        Cookie[] cookies = request.getCookies();
        if(cookies != null && cookies.length > 0) {
            sb.append("\n--> Cookies :");
            for(Cookie cookie : cookies)
                sb.append("\n    - " + padRight(cookie.getName(), 30) + " : " + cookie.getValue());
        }

        sb.append("\n--> Content Type       : " + request.getContentType());
        sb.append("\n--> Character Encoding : " + request.getCharacterEncoding());
        sb.append("\n--> Client IP     : " + ServletUtils.getClientIpAddress(request));

        // https://stackoverflow.com/a/26870975
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        Browser browser = userAgent.getBrowser();
        Version browserVersion = userAgent.getBrowserVersion();
        OperatingSystem os = userAgent.getOperatingSystem();

        sb.append("\n--> Browser       : ");
        sb.append("\n    - Name              :  " + browser.getName());
        sb.append("\n    - Version           :  " + browserVersion);
        sb.append("\n    - Manufacturer      :  " + browser.getManufacturer());
        sb.append("\n    - Rendering Engine  :  " + browser.getRenderingEngine().getName());

        sb.append("\n--> Operating System :");
        sb.append("\n    - Name              :  " + os.getName());
        sb.append("\n    - Device Type       :  " + os.getDeviceType());
        sb.append("\n    - Manufacturer      :  " + os.getManufacturer());
        sb.append("\n    - Is mobile device? :  " + os.isMobileDevice());

        sb.append("\n---------------------------------");

        //System.out.println(sb.toString());
        logger.info("HTTP Request Audit Logger", sb.toString());
    }

    public static final String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }

    public static final String padLeft(String s, int n) {
        return String.format("%" + n + "s", s);
    }

    private static final void logResponse(final ResponseWrapper response) {
        StringBuilder sb = new StringBuilder();
        sb.append("\n---------------------------------");
        sb.append("\nHTTP RESPONSE");
        sb.append("\n---------------------------------");
        sb.append("\n--> Content Type       :       " + response.getContentType());
        sb.append("\n--> Character Encoding :       " + response.getCharacterEncoding());

        Collection<String> headerCollections = response.getHeaderNames();
        if(headerCollections != null && headerCollections.size() > 0) {
            sb.append("\n--> Headers :");
            for(String key : headerCollections)
                sb.append("\n    - " + padRight(key, 30) + " : " + response.getHeader(key));
        }

        Collection<Cookie> cookies = response.getCookies();
        if(cookies != null && cookies.size() > 0) {
            sb.append("\n--> Cookies :");
            for(Cookie cookie : cookies)
                sb.append("\n    - " + padRight(cookie.getName(), 30) + " : " + cookie.getValue());
        }

        sb.append("\n--> Response Code      :       " + response.getStatus());

        try {
            String responseBody = new String(response.toByteArray(), response.getCharacterEncoding());
            if(responseBody != null && !responseBody.isEmpty())
                sb.append("\n--> Response Body      :       " + responseBody);
        }
        catch(Exception e) {
        }
        sb.append("\n---------------------------------");

        //System.out.println(sb.toString());
        logger.info("HTTP Response Audit Logger", sb.toString());
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request = new RequestWrapper((HttpServletRequest) request);
        response = new ResponseWrapper((HttpServletResponse) response);

        logRequest((RequestWrapper) request);

        chain.doFilter(request, response);

        logResponse((ResponseWrapper) response);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
