/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.logger;

import com.aliyun.openservices.log.common.LogItem;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * AliyunSLS
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:18:53 PM
 */
@Component
public class AliyunSLS extends BaseAliyunSLS {
    
    @Override
    public void debug(String topic, String message) {
        try {
            List<LogItem> logGroup = new ArrayList<LogItem>();

            LogItem logItem = new LogItem((int) (new Date().getTime() / 1000));
            logItem.PushBack("level", "DEBUG");
            logItem.PushBack("class", clazz.getName());
            logItem.PushBack("message", message);

            logGroup.add(logItem);

            client.PutLogs(project, logStore, topic, logGroup, "");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void debug(String message) {
        debug("", message);
    }

    @Override
    public void info(String topic, String message) {
        try {
            logger.info((topic != null && !topic.isEmpty() ? topic + " - " : "") + message);
            
            List<LogItem> logGroup = new ArrayList<LogItem>();

            LogItem logItem = new LogItem((int) (new Date().getTime() / 1000));
            logItem.PushBack("level", "INFO");
            logItem.PushBack("class", clazz.getName());
            logItem.PushBack("message", message);

            logGroup.add(logItem);

            client.PutLogs(project, logStore, topic, logGroup, "");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    @Override
    public void info(String message) {
        info("", message);
    }

    @Override
    public void warn(String topic, String message) {
        try {
            List<LogItem> logGroup = new ArrayList<LogItem>();

            LogItem logItem = new LogItem((int) (new Date().getTime() / 1000));
            logItem.PushBack("level", "WARN");
            logItem.PushBack("class", clazz.getName());
            logItem.PushBack("message", message);

            logGroup.add(logItem);

            client.PutLogs(project, logStore, topic, logGroup, "");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void warn(String message) {
        warn("", message);
    }

    @Override
    public void error(String topic, String message, Throwable t) {
        try {
            List<LogItem> logGroup = new ArrayList<LogItem>();

            LogItem logItem = new LogItem((int) (new Date().getTime() / 1000));
            logItem.PushBack("level", "ERROR");
            logItem.PushBack("class", clazz.getName());
            logItem.PushBack("message", message);
            
            if(t != null) {
                StringWriter sw = new StringWriter();
                t.printStackTrace(new PrintWriter(sw));
                String stackTraceAsString = sw.toString();
                System.out.println(stackTraceAsString);
                
                logItem.PushBack("exception", t.getMessage());
                logItem.PushBack("stacktrace", stackTraceAsString);
            }

            logGroup.add(logItem);

            client.PutLogs(project, logStore, topic, logGroup, "");
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public void error(String message, Throwable t) {
        error("", message, t);
    }

    @Override
    public void error(Throwable t) {
        error("", t.getMessage(), t);
    }
}
