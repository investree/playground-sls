/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.logger;

/**
 * AsyncAliyunSLSFactory
 *
 * @author Fariz Fadian
 * @since May 30, 2020 - 4:21:22 AM
 */
public final class AsyncAliyunSLSFactory extends BaseAliyunSLSFactory {
    
    public static AsyncAliyunSLS getLogger(Class<?> clazz) {
        AsyncAliyunSLS asyncSLS = getContext().getBean(AsyncAliyunSLS.class);
        asyncSLS.setClazz(clazz);
        return asyncSLS;
    }
}