/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.logger;

import id.investree.sdk.bean.AliyunSLSBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * BaseAliyunSLSFactory
 *
 * @author Fariz Fadian
 * @since May 30, 2020 - 4:22:03 AM
 */
public class BaseAliyunSLSFactory {
    
    private static ApplicationContext context;

    protected static ApplicationContext getContext() {
        if(context == null)
            context = new AnnotationConfigApplicationContext(AliyunSLSBean.class);
        return context;
    }

}