/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.logger;

/**
 * AliyunSLSFactory
 *
 * @author Fariz Fadian
 * @since May 30, 2020 - 2:52:01 AM
 */
public final class AliyunSLSFactory extends BaseAliyunSLSFactory {

    public static AliyunSLS getLogger(Class<?> clazz) {
        AliyunSLS sls = getContext().getBean(AliyunSLS.class);
        sls.setClazz(clazz);
        return sls;
    }
}
