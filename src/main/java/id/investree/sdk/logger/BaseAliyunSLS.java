/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk.logger;

import id.investree.sdk.config.AliyunSLSConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BaseAliyunSLS
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:33:44 PM
 */
public abstract class BaseAliyunSLS extends AliyunSLSConfiguration {
    
    protected Logger logger;
    protected Class<?> clazz;

    protected void setClazz(Class<?> clazz) {
        this.clazz = clazz;
        this.logger = LoggerFactory.getLogger(clazz);
    }

    protected Class<?> getClazz() {
        return clazz;
    }

    protected void setLogger(Logger logger) {
        this.logger = logger;
    }

    protected Logger getLogger() {
        return logger;
    }

    public abstract void debug(String topic, String message);
    public abstract void debug(String message);
    
    public abstract void info(String topic, String message);
    public abstract void info(String message);
    
    public abstract void warn(String topic, String message);
    public abstract void warn(String message);
    
    public abstract void error(String topic, String message, Throwable t);
    public abstract void error(String message, Throwable t);
    public abstract void error(Throwable t);
}
