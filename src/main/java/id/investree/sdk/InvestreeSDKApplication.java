/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.investree.sdk;

import id.investree.sdk.logger.AsyncAliyunSLS;
import id.investree.sdk.logger.AsyncAliyunSLSFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * InvestreeSDKApplication
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:02:06 AM
 */
@SpringBootApplication
public class InvestreeSDKApplication implements CommandLineRunner {
    
    //private static final AliyunSLS logger = AliyunSLSFactory.getLogger(InvestreeSDKApplication.class);
    private static final AsyncAliyunSLS logger = AsyncAliyunSLSFactory.getLogger(InvestreeSDKApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(InvestreeSDKApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("App Running...");
    }
}
