/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.ToString;

/**
 * Employee
 *
 * @author Fariz Fadian
 * @since May 26, 2020 - 11:04:44 AM
 */
@ToString
@Data
@JsonIgnoreProperties
@Entity
@Table(name = "EMPLOYEE")
public class Employee {
    
    @JsonProperty("id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, length = 10)
    private int id;

    @JsonProperty("name")
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;
    
    @JsonProperty("address")
    @Column(name = "ADDRESS", nullable = true, length = 100)
    private String address;
    
    @JsonProperty("phone")
    @Column(name = "PHONE", nullable = true, length = 100)
    private String phone;

    public Employee(int id, String name, String address, String phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public Employee() {
    }
}