/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package id.investree.sdk.bean;

import id.investree.sdk.logger.AliyunSLS;
import id.investree.sdk.logger.AsyncAliyunSLS;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * AliyunSLSBean
 * 
 * @author Fariz Fadian
 * @since May 30, 2020 - 4:05:43 AM
 */
@Configuration
@EnableAsync
@Scope("prototype")
@PropertySource("classpath:application.properties")
public class AliyunSLSBean {

    @Bean
    @Scope("prototype")
    public AliyunSLS aliyunSLS() {
        return new AliyunSLS();
    }
    
    @Bean
    @Scope("prototype")
    public AsyncAliyunSLS asyncAliyunSLS() {
        return new AsyncAliyunSLS();
    }
    
    @Bean
    public TaskExecutor aliyunSLSTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("Aliyun-SLS-TaskExecutor");
        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(1);
        executor.setQueueCapacity(1000);
        executor.setDaemon(true);
        return executor;
    }
}